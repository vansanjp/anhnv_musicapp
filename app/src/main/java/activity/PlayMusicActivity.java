package activity;

import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.List;

import custom.CircuclarSeekBar;
import custom.SongListViewAdapter;
import listener.PlayPauseListener;
import model.Song;
import thuctaprikkei.beautymusic.R;

public class PlayMusicActivity extends AppCompatActivity {

    public static CircuclarSeekBar sCircleSeekbar;
    public static TextView sDurationTextView;
    public static ImageView sSongImageView;
    public static ImageView sPlayPauseBtn;

    public static Handler sHandler = new Handler();
    private List<Song> mSongList = MainActivity.sSongList;
    private int mPosition = MainActivity.sPosition;
    private boolean mSuffleCheck = false, mRepeatCheck = false;

    private ListView mSongListView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_music);

        sDurationTextView = (TextView)findViewById(R.id.play_music_duration);
        if(MainActivity.sMp != null){
            setDuration(MainActivity.sMp.getDuration());
        }
        sSongImageView = (ImageView)findViewById(R.id.play_music_song_image);
        setSongImage();
        sCircleSeekbar = (CircuclarSeekBar) findViewById(R.id.play_music_circle_seek_bar);
        setCircleSeekBar();
        sPlayPauseBtn= (ImageView)findViewById(R.id.play_music_play_pause_btn);
        if(MainActivity.sMp.isPlaying()){
            sPlayPauseBtn.setImageResource(R.drawable.ic_pause);
        } else{
            sPlayPauseBtn.setImageResource(R.drawable.ic_play);
        }
        sPlayPauseBtn.setOnClickListener(new PlayPauseListener(this));
        mSongListView = (ListView)findViewById(R.id.play_music_song_list_view);
        SongListViewAdapter adapter = new SongListViewAdapter(this,mSongList);
        adapter.setViewId(R.layout.song_item);
        mSongListView.setAdapter(adapter);
    }

    public static Runnable updateSongTime = new Runnable() {
        @Override
        public void run() {
            if(MainActivity.sMp != null){
                int current = MainActivity.sMp.getCurrentPosition();
                setDuration(current);
                sCircleSeekbar.setProgress(current);
                sHandler.postDelayed(updateSongTime,100);
            }
        }
    };

    public static void setCircleSeekBar(){
        if(sCircleSeekbar == null){
            return;
        }
        if(MainActivity.sMp != null){
            int length = MainActivity.sMp.getDuration();
            int current = MainActivity.sMp.getCurrentPosition();
            sCircleSeekbar.setMax(length);
            sCircleSeekbar.setProgress(current);
            sHandler.postDelayed(updateSongTime,100);
        } else{
            sCircleSeekbar.setMax(1000);
        }
    }



    public static void setDuration(int duration){
        if(sDurationTextView == null){
            return;
        }
        int minute = duration/(60*1000);
        int second = (duration/1000)%60;
        String s = String.format("%d:%02d",minute,second);
        sDurationTextView.setText(s);
    }

    public static void setSongImage(){
        Song tempSong;
        if(sSongImageView == null){
            return;
        }
        if((tempSong = MainActivity.sCurrentSong) != null){
            Bitmap bm;
            if((bm = tempSong.getImagePath()) != null){
                sSongImageView.setImageBitmap(bm);
            } else{
                sSongImageView.setImageResource(R.drawable.default_music_icon);
            }
        } else{
            sSongImageView.setImageResource(R.drawable.default_music_icon);
        }

    }
}
