package activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.List;

import audio.AudioUtils;
import fragment.ArtistFragment;
import listener.PlayPauseListener;
import custom.CustomPagerAdapter;
import fragment.AlbumFragment;
import fragment.SongFragment;
import model.Album;
import model.Artist;
import model.Song;
import thuctaprikkei.beautymusic.R;

public class MainActivity extends AppCompatActivity {
    // Declare static objects
    public static MediaPlayer sMp;       // Mediaplayer
    public static List<Song> sSongList;  // list of current songs
    public static int sPosition = 0;     // current position of song in list
    public static Song sCurrentSong;
    public static boolean sStart = false;
    public static Handler myHandler = new Handler();

    //Declare private objects
    private List<Song> mAllSong;
    private List<Album> mAllAlbum;
    private List<Artist> mAllArtist;

    // Declare private view
    private Toolbar mToolbar;
    private ViewPager mViewPager;
    private CustomPagerAdapter mPagerAdapter;
    private RelativeLayout mPlayAreaLayout;

    // Declare public view
    public static ImageView sMainMusicIconIv;         //playing song's icon
    public static TextView sMainSongTv,sMainArtistTv; //playing song, playing song's artist
    public static ImageView sPlayPauseIv;
    public static SeekBar sSeekbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mToolbar = (Toolbar)findViewById(R.id.main_toolbar);
        setSupportActionBar(mToolbar);

        mAllSong = AudioUtils.getAllSongs(this);
        mAllAlbum = AudioUtils.getAllAlbums(this);
        mAllArtist = AudioUtils.getAllArtist(this);

        sSongList = mAllSong;

        mViewPager = (ViewPager)findViewById(R.id.main_pager);
        mPagerAdapter = new CustomPagerAdapter(getSupportFragmentManager());
        mPagerAdapter.putSongList(mAllSong);
        mPagerAdapter.putArtistList(mAllArtist);
        mPagerAdapter.putAlbumList(mAllAlbum);
        mViewPager.setAdapter(mPagerAdapter);
        mPlayAreaLayout = (RelativeLayout)findViewById(R.id.playing_music_area);
        mPlayAreaLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(sMp == null){
                    return;
                }
                Intent i = new Intent(MainActivity.this,PlayMusicActivity.class);
                startActivity(i);
            }
        });

        // Use for android version 23 or higher
        ((ViewPager.LayoutParams) (findViewById(R.id.main_tab_strip)).getLayoutParams()).isDecor = true;

        sSeekbar = (SeekBar)findViewById(R.id.main_seek_bar);
        setSeekBar();

        sMainMusicIconIv = (ImageView)findViewById(R.id.music_icon);
        sMainSongTv = (TextView)findViewById(R.id.song_text);
        sMainArtistTv = (TextView)findViewById(R.id.artist_text);
        sPlayPauseIv = (ImageView)findViewById(R.id.button_play_pause);
        sPlayPauseIv.setOnClickListener(new PlayPauseListener(this));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_toolbar_menu,menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_search:
                return true;
            case R.id.action_grid_view:
                if(mViewPager.getCurrentItem() == 0){
                    Fragment f = getSupportFragmentManager().findFragmentByTag
                            ("android:switcher:"+R.id.main_pager+":"+mViewPager.getCurrentItem());
                    if(f != null){
                        SongFragment sf = (SongFragment)f;
                        sf.setGridView();
                    }
                } else if(mViewPager.getCurrentItem() == 1){
                    Fragment f = getSupportFragmentManager().findFragmentByTag
                            ("android:switcher:"+R.id.main_pager+":"+mViewPager.getCurrentItem());
                    if(f != null){
                        AlbumFragment af = (AlbumFragment)f;
                        af.setGridView();
                    }
                } else if(mViewPager.getCurrentItem() == 2){
                    Fragment f = getSupportFragmentManager().findFragmentByTag
                            ("android:switcher:"+R.id.main_pager+":"+mViewPager.getCurrentItem());
                    if(f != null){
                        ArtistFragment arf = (ArtistFragment)f;
                        arf.setGridView();
                    }
                }
                return true;
            case R.id.action_list_view:
                if(mViewPager.getCurrentItem() == 0){
                    Fragment f = getSupportFragmentManager().findFragmentByTag
                            ("android:switcher:"+R.id.main_pager+":"+mViewPager.getCurrentItem());
                    if(f != null){
                        SongFragment sf = (SongFragment)f;
                        sf.setListView();
                    }
                } else if(mViewPager.getCurrentItem() == 1){
                    Fragment f = getSupportFragmentManager().findFragmentByTag
                            ("android:switcher:"+R.id.main_pager+":"+mViewPager.getCurrentItem());
                    if(f != null){
                        AlbumFragment af = (AlbumFragment)f;
                        af.setListView();
                    }
                } else if(mViewPager.getCurrentItem() == 2){
                    Fragment f = getSupportFragmentManager().findFragmentByTag
                            ("android:switcher:"+R.id.main_pager+":"+mViewPager.getCurrentItem());
                    if(f != null){
                        ArtistFragment arf = (ArtistFragment)f;
                        arf.setListView();
                    }
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public static void playMusic(final Context context){
        if(sSongList != null){
            sCurrentSong = sSongList.get(sPosition);
            displayPlayingMusicArea(sCurrentSong);
            if(sMp != null && sMp.isPlaying()){
                sMp.stop();
            }
            sMp = MediaPlayer.create(context, Uri.parse(sCurrentSong.getPath()));
            sMp.start();
            setSeekBar();
            PlayMusicActivity.setCircleSeekBar();
            PlayMusicActivity.setSongImage();
            sMp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mediaPlayer) {
                    if(sPosition < sSongList.size()-1){
                        sPosition++;
                        playMusic(context);
                    } else{
                        sPosition = 0;
                        playMusic(context);
                    }
                }
            });
            sPlayPauseIv.setImageResource(R.drawable.ic_pause);
            if(PlayMusicActivity.sPlayPauseBtn != null){
                PlayMusicActivity.sPlayPauseBtn.setImageResource(R.drawable.ic_pause);
            }
            sStart = true;
        }
    }


    public static void displayPlayingMusicArea(Song tempSong){
        sMainSongTv.setText(tempSong.getName());
        sMainArtistTv.setText(tempSong.getArtist());
        Bitmap songImagePath = tempSong.getImagePath();
        if(songImagePath == null){ //tempSong doesn't have album image
            sMainMusicIconIv.setImageResource(R.drawable.music_note);
        } else{ // tempSong has album image
            sMainMusicIconIv.setImageBitmap(songImagePath);
        }
    }

    public static Runnable updateSongTime = new Runnable() {
        @Override
        public void run() {
            if(sMp != null){
                int current = sMp.getCurrentPosition();
                sSeekbar.setProgress(current);
                myHandler.postDelayed(updateSongTime,100);
            }
        }
    };

    private static void setSeekBar(){
        if(sMp != null){
            sSeekbar.setClickable(false);
            int length = sMp.getDuration();
            int current = sMp.getCurrentPosition();
            sSeekbar.setMax(length);
            sSeekbar.setProgress(current);
            myHandler.postDelayed(updateSongTime,100);
        } else{
            sSeekbar.setMax(1000);
        }
    }
}
