package fragment;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import audio.AudioUtils;
import custom.ArtistListViewAdapter;
import custom.CustomPagerAdapter;
import custom.SongListViewAdapter;
import model.Artist;
import model.Song;
import thuctaprikkei.beautymusic.R;

/**
 * Created by vietanh on 9/23/16.
 */
public class ArtistFragment extends android.support.v4.app.Fragment {
    private List<Artist> artistList;
    private Context mContext;

    private GridView mArtistGridView;
    private ListView mArtistListView;

    private ArtistListViewAdapter adapter;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_artists_tab,container,false);

        artistList = new ArrayList<Artist>();
        mContext = getActivity();
        artistList = getArguments().getParcelableArrayList(CustomPagerAdapter.ARTIST_LIST_KEY);
        adapter = new ArtistListViewAdapter(mContext,artistList);

        mArtistListView = (ListView)rootView.findViewById(R.id.artist_list);
        mArtistGridView = (GridView)rootView.findViewById(R.id.artist_grid_list);

        setListView();
        return rootView;
    }

    public void setListView(){
        mArtistGridView.setVisibility(View.GONE);
        mArtistListView.setVisibility(View.VISIBLE);
        adapter.setViewId(R.layout.artist_item);
        mArtistListView.setAdapter(adapter);
    }

    public void setGridView(){
        mArtistListView.setVisibility(View.GONE);
        mArtistGridView.setVisibility(View.VISIBLE);
        adapter.setViewId(R.layout.artist_grid_item);
        mArtistGridView.setAdapter(adapter);
    }
}
