package fragment;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import audio.AudioUtils;
import custom.AlbumListViewAdapter;
import custom.CustomPagerAdapter;
import custom.SongListViewAdapter;
import model.Album;
import model.Song;
import thuctaprikkei.beautymusic.R;

/**
 * Created by vietanh on 9/23/16.
 */
public class AlbumFragment extends android.support.v4.app.Fragment {

    private ListView mAlbumListView;
    private GridView mAlbumGridView;

    private AlbumListViewAdapter adapter;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_albums_tab,container,false);

        List<Album> albumList = new ArrayList<Album>();
        Context context = getActivity();
        albumList = getArguments().getParcelableArrayList(CustomPagerAdapter.ALBUM_LIST_KEY);
        adapter = new AlbumListViewAdapter(albumList,context);

        mAlbumListView = (ListView)rootView.findViewById(R.id.album_list);
        mAlbumGridView = (GridView)rootView.findViewById(R.id.album_grid_list);

        setListView();
        return rootView;
    }

    public void setListView(){
        mAlbumGridView.setVisibility(View.GONE);
        mAlbumListView.setVisibility(View.VISIBLE);
        adapter.setViewId(R.layout.album_item);
        mAlbumListView.setAdapter(adapter);
    }

    public void setGridView(){
        mAlbumListView.setVisibility(View.GONE);
        mAlbumGridView.setVisibility(View.VISIBLE);
        adapter.setViewId(R.layout.album_grid_item);
        mAlbumGridView.setAdapter(adapter);
    }
}
