package fragment;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import activity.MainActivity;
import audio.AudioUtils;
import custom.CustomPagerAdapter;
import custom.SongListViewAdapter;
import model.Song;
import thuctaprikkei.beautymusic.R;

/**
 * Created by vietanh on 9/22/16.
 */
public class SongFragment extends android.support.v4.app.Fragment {
    private List<Song> songList;
    private Context mContext;


    private ListView mSongListView;
    private GridView mSongGridView;
    private SongListViewAdapter adapter;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_songs_tab,container,false);

        songList = new ArrayList<Song>();
        mContext = getActivity();
        songList = getArguments().getParcelableArrayList(CustomPagerAdapter.SONG_LIST_KEY);
        adapter = new SongListViewAdapter(mContext,songList);

        mSongListView = (ListView)rootView.findViewById(R.id.song_list);
        mSongGridView = (GridView)rootView.findViewById(R.id.song_grid_list);

        setListView();
        return rootView;
    }


    public void setListView(){
        mSongGridView.setVisibility(View.GONE);
        mSongListView.setVisibility(View.VISIBLE);
        adapter.setViewId(R.layout.song_item);
        mSongListView.setAdapter(adapter);
    }

    public void setGridView(){
        mSongListView.setVisibility(View.GONE);
        mSongGridView.setVisibility(View.VISIBLE);
        adapter.setViewId(R.layout.song_grid_item);
        mSongGridView.setAdapter(adapter);
    }
}
