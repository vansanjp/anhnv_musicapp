package listener;

import android.content.Context;
import android.media.MediaPlayer;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ImageView;

import activity.MainActivity;
import thuctaprikkei.beautymusic.R;

/**
 * Created by vietanh on 10/5/16.
 */
public class PlayPauseListener implements View.OnClickListener {
    Context mContext;
    public PlayPauseListener(Context context) {
        mContext = context;
    }

    @Override
    public void onClick(View view) {
        MediaPlayer mp = MainActivity.sMp;
        ImageView playPauseIV = (ImageView)view;
        if(mp != null && mp.isPlaying()){
            mp.pause();
            playPauseIV.setImageResource(R.drawable.ic_play);
        } else{
            if(MainActivity.sStart){
                mp.start();
            } else{
                MainActivity.playMusic(mContext);
            }
            playPauseIV.setImageResource(R.drawable.ic_pause);
        }
    }
}
