package custom;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import model.Artist;
import thuctaprikkei.beautymusic.R;

/**
 * Created by vietanh on 10/6/16.
 */
public class ArtistListViewAdapter extends BaseAdapter {
    private Context mContext;
    private List<Artist> mArtistList;
    private int mViewID;

    public ArtistListViewAdapter(Context context, List<Artist> artistList){
        mContext = context;
        mArtistList = artistList;
    }

    public void setViewId(int viewId){
        mViewID = viewId;
    }

    @Override
    public int getCount() {
        if(mArtistList == null){
            return 0;
        }
        return mArtistList.size();
    }

    @Override
    public Object getItem(int i) {
        return mArtistList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View rawView;
        LayoutInflater inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        rawView = inflater.inflate(mViewID,null);

        Artist tempArtist = mArtistList.get(i);

        TextView artistName = (TextView)rawView.findViewById(R.id.artist_item_name);
        artistName.setText(tempArtist.getArtistName());

        TextView numberAlbum = (TextView)rawView.findViewById(R.id.artist_item_number_albums);
        numberAlbum.setText(tempArtist.getAlbumList().size()+" album(s)");

        TextView numberSong = (TextView)rawView.findViewById(R.id.artist_item_number_songs);
        numberSong.setText(tempArtist.getSongList().size()+" song(s)");

        return rawView;
    }
}
