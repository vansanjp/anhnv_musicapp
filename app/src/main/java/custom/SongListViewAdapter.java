package custom;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.ThumbnailUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;
import java.util.zip.Inflater;

import activity.MainActivity;
import audio.AudioUtils;
import model.Song;
import thuctaprikkei.beautymusic.R;

/**
 * Created by vietanh on 9/22/16.
 */
public class SongListViewAdapter extends BaseAdapter {
    private int mViewId;
    private Context mContext;
    private List<Song> mSongList;

    public SongListViewAdapter(Context context, List<Song> songList){
        mContext = context;
        mSongList = songList;
    }

    public void setViewId(int viewRes){
        mViewId = viewRes;
    }

    @Override
    public int getCount() {
        if(mSongList == null){
            return 0;
        }
        return mSongList.size();
    }

    @Override
    public Song getItem(int i) {
        return mSongList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View rowView;
        Song tempSong = mSongList.get(i);
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        rowView = inflater.inflate(mViewId,null);

        TextView name_tv = (TextView)rowView.findViewById(R.id.song_item_name_text);
        name_tv.setText(tempSong.getName());

        TextView artist_tv = (TextView)rowView.findViewById(R.id.song_item_artist_text);
        artist_tv.setText(tempSong.getArtist());

        ImageView icon_iv = (ImageView)rowView.findViewById(R.id.song_item_icon);
        Bitmap songImagePath = tempSong.getImagePath();
        if(songImagePath == null){ //tempSong doesn't have album image
            icon_iv.setImageResource(R.drawable.music_note);
        } else{ // tempSong has album image
            icon_iv.setImageBitmap(songImagePath);
        }

        final int x = i;

        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(x == MainActivity.sPosition){
                    return;
                }
                MainActivity.sPosition = x;
                MainActivity.playMusic(mContext);
            }
        });
        return rowView;
    }
}
