package custom;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;

import java.util.ArrayList;
import java.util.List;

import activity.MainActivity;
import fragment.AlbumFragment;
import fragment.ArtistFragment;
import fragment.SongFragment;
import model.Album;
import model.Artist;
import model.Song;

/**
 * Created by vietanh on 9/23/16.
 */
public class CustomPagerAdapter extends FragmentPagerAdapter {
    public static final String SONG_LIST_KEY = "song_key";
    public static final String ALBUM_LIST_KEY = "album_key";
    public static final String ARTIST_LIST_KEY = "artist_key";

    private String mTabTitle[] = {"SONGS","ALBUMS","ARTISTS"};
    private Fragment[] mFragmentArray ;
    public CustomPagerAdapter(FragmentManager fm){
        super(fm);
        mFragmentArray = new Fragment[3];
        mFragmentArray[0] = new SongFragment();
        mFragmentArray[1] = new AlbumFragment();
        mFragmentArray[2] = new ArtistFragment();
    }

    public void putSongList(List<Song> songList){
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(SONG_LIST_KEY,(ArrayList<Song>)songList);
        mFragmentArray[0].setArguments(bundle);
    }

    public void putArtistList(List<Artist> artistList){
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(ARTIST_LIST_KEY,(ArrayList<Artist>)artistList);
        mFragmentArray[2].setArguments(bundle);
    }

    public void putAlbumList(List<Album> albumList){
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(ALBUM_LIST_KEY,(ArrayList<Album>)albumList);
        mFragmentArray[1].setArguments(bundle);
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentArray[position];
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mTabTitle[position];
    }
}
