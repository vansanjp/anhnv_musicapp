package custom;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.List;

import audio.AudioUtils;
import model.Album;
import thuctaprikkei.beautymusic.R;

/**
 * Created by vietanh on 9/29/16.
 */
public class AlbumListViewAdapter extends BaseAdapter {
    private List<Album> mAlbumList;
    private Context mContext;
    private int layoutId;

    public AlbumListViewAdapter(List<Album> albumList, Context context){
        mAlbumList = albumList;
        mContext = context;
    }

    public void setViewId(int viewId){
        layoutId = viewId;
    }

    @Override
    public int getCount() {
        if(mAlbumList == null){
            return 0;
        }
        return mAlbumList.size();
    }

    @Override
    public Object getItem(int i) {
        return mAlbumList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View rawView;
        LayoutInflater inflater = (LayoutInflater)
                mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        rawView = inflater.inflate(layoutId,null);

        Album tempAlbum = mAlbumList.get(i);

        TextView albumNameTextView = (TextView)rawView.findViewById(R.id.album_item_name);
        albumNameTextView.setText(tempAlbum.getAlbumName());

        TextView artistNameTextView = (TextView)rawView.findViewById(R.id.album_item_artist);
        artistNameTextView.setText(tempAlbum.getAlbumArtist());

        ImageView albumIconImageView = (ImageView)rawView.findViewById(R.id.album_item_icon);
        Bitmap bm = AudioUtils.getSongImagePath(tempAlbum.getAlbumId(),mContext);
        if(bm != null){
            albumIconImageView.setImageBitmap(bm);
        } else{
            albumIconImageView.setImageResource(R.drawable.default_album_icon);
        }
        return rawView;
    }
}
