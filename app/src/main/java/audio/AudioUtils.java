package audio;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;

import java.util.ArrayList;
import java.util.List;

import model.Album;
import model.Artist;
import model.Song;

/**
 * Created by vietanh on 9/22/16.
 */
public class AudioUtils {
    public static List<Song> getAllSongs(Context context){
        List<Song> allSongs = new ArrayList<Song>();

        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI; // Media source
        String projection[] = {
            MediaStore.Audio.Media.ARTIST,
            MediaStore.Audio.Media.DISPLAY_NAME,
            MediaStore.Audio.Media.DURATION, // Duration of song
            MediaStore.Audio.Media.DATA,     // Path of song's mp3 file
            MediaStore.Audio.Media.ALBUM_ID  // Album include song
        }; // Song's attributes array
        String whereQuery = MediaStore.Audio.Media.IS_MUSIC + " != 0";
        String orderByQuery = MediaStore.Audio.Media.DISPLAY_NAME + " ASC";

        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(uri,projection,whereQuery,null,orderByQuery);

        copyDataFromSongCursor(c,allSongs,context);

        return allSongs;
    }

    public static List<Song> getAllSongs(Context context, int albumId){
        List<Song> albumSongs = new ArrayList<Song>();
        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI; // Media source
        String projection[] = {
                MediaStore.Audio.Media.ARTIST,
                MediaStore.Audio.Media.DISPLAY_NAME,
                MediaStore.Audio.Media.DURATION, // Duration of song
                MediaStore.Audio.Media.DATA,     // Path of song's mp3 file
                MediaStore.Audio.Media.ALBUM_ID  // Album include song
        }; // Song's attributes array
        String whereQuery = MediaStore.Audio.Media.IS_MUSIC + " != 0 AND "+
                            MediaStore.Audio.Media.ALBUM_ID+" = "+albumId;
        String orderByQuery = MediaStore.Audio.Media.DISPLAY_NAME + " ASC";

        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(uri,projection,whereQuery,null,orderByQuery);

        copyDataFromSongCursor(c,albumSongs,context);

        return albumSongs;
    }

    public static List<Song> getAllSongs(Context context, String artist){
        List<Song> artistSongs = new ArrayList<Song>();
        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI; // Media source
        String projection[] = {
                MediaStore.Audio.Media.ARTIST,
                MediaStore.Audio.Media.DISPLAY_NAME,
                MediaStore.Audio.Media.DURATION, // Duration of song
                MediaStore.Audio.Media.DATA,     // Path of song's mp3 file
                MediaStore.Audio.Media.ALBUM_ID  // Album include song
        }; // Song's attributes array
        String whereQuery = MediaStore.Audio.Media.IS_MUSIC + " != 0 AND "+
                MediaStore.Audio.Media.ARTIST+" = \""+artist+"\"";
        String orderByQuery = MediaStore.Audio.Media.DISPLAY_NAME + " ASC";

        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(uri,projection,whereQuery,null,orderByQuery);

        copyDataFromSongCursor(c,artistSongs,context);

        return artistSongs;
    }

    public static List<Album> getAllAlbums(Context context){
        List<Album> allAlbums = new ArrayList<Album>();

        Uri uri = MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI;
        String projection[] = {
                MediaStore.Audio.Albums._ID,
                MediaStore.Audio.Albums.ALBUM,
                MediaStore.Audio.Albums.ARTIST
        };
        String orderByQuerry = MediaStore.Audio.Albums.ALBUM+" ASC";

        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(uri, projection, null,null,orderByQuerry);

        copyDataFromAlbumCursor(c,allAlbums,context);

        return allAlbums;
    }

    public static List<Album> getAllAlbums(Context context, String artist){
        List<Album> allArtistAlbums = new ArrayList<Album>();

        Uri uri = MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI;
        String projection[] = {
                MediaStore.Audio.Albums._ID,
                MediaStore.Audio.Albums.ALBUM,
                MediaStore.Audio.Albums.ARTIST
        };
        String selection = MediaStore.Audio.Albums.ARTIST+" = \""+artist+"\"";
        String orderByQuerry = MediaStore.Audio.Albums.ALBUM+" ASC";

        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(uri, projection, selection,null,orderByQuerry);

        copyDataFromAlbumCursor(c,allArtistAlbums,context);

        return allArtistAlbums;
    }

    public static List<Artist> getAllArtist(Context context){
        List<Artist> allArtists = new ArrayList<Artist>();

        Uri uri = MediaStore.Audio.Artists.EXTERNAL_CONTENT_URI;

        String projection[] = {MediaStore.Audio.Artists.ARTIST};
        String orderByQuery = MediaStore.Audio.Artists.ARTIST+" ASC";

        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(uri,projection,null,null,orderByQuery);

        copyDataFromArtistCursor(c,allArtists,context);

        return allArtists;
    }

    public static Bitmap getSongImagePath(int albumId, Context context){
        final Uri ART_CONTENT_URI = Uri.parse("content://media/external/audio/albumart");
        Uri albumArtUri = ContentUris.withAppendedId(ART_CONTENT_URI, albumId);

        Bitmap bitmap = null;
        try {
            bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(),albumArtUri);
            int width = bitmap.getWidth();
            int height = bitmap.getHeight();

            float bitmapRatio = (float) width / (float) height;
            if (bitmapRatio > 1) {
                width = 150;
                height = (int) (width / bitmapRatio);
            } else {
                height = 150;
                width = (int) (height * bitmapRatio);
            }

            bitmap = Bitmap.createScaledBitmap(bitmap,width,height,true);
        }catch (Exception e){
            e.printStackTrace();
        }
        return bitmap;
    }

    private static void copyDataFromArtistCursor(Cursor sourceCursor, List<Artist> destArtistList, Context context){
        if(sourceCursor == null){
            destArtistList = null;
            return;
        } else{
            while(sourceCursor.moveToNext()){
                String artist = sourceCursor.getString
                        (sourceCursor.getColumnIndex(MediaStore.Audio.Artists.ARTIST));

                List<Song> allArtistSong = getAllSongs(context,artist);
                List<Album> allArtistAlbum = getAllAlbums(context,artist);

                Artist tempArtist = new Artist(artist, allArtistSong, allArtistAlbum);

                destArtistList.add(tempArtist);
            }
        }
    }

    private static void copyDataFromAlbumCursor(Cursor sourceCursor, List<Album> destAlbumList,Context context){
        if(sourceCursor == null){
            destAlbumList = null;
            return;
        } else{
            while(sourceCursor.moveToNext()){
                int albumId = sourceCursor.getInt
                        (sourceCursor.getColumnIndex(MediaStore.Audio.Albums._ID));
                String albumName = sourceCursor.getString
                        (sourceCursor.getColumnIndex(MediaStore.Audio.Albums.ALBUM));
                String albumArtist = sourceCursor.getString
                        (sourceCursor.getColumnIndex(MediaStore.Audio.Albums.ARTIST));
                List<Song> albumSongList = getAllSongs(context,albumId);
                Album tempAlbum = new Album(albumId,albumName,albumArtist,albumSongList);
                destAlbumList.add(tempAlbum);
            }
        }
    }

    private static void copyDataFromSongCursor(Cursor sourceCursor, List<Song> destSongList, Context context){
        if(sourceCursor == null){
            destSongList = null;
            return;
        } else {
            while (sourceCursor.moveToNext()) {
                String artist = sourceCursor.getString
                        (sourceCursor.getColumnIndex(MediaStore.Audio.Media.ARTIST));
                String name = sourceCursor.getString
                        (sourceCursor.getColumnIndex(MediaStore.Audio.Media.DISPLAY_NAME));
                int duration = sourceCursor.getInt
                        (sourceCursor.getColumnIndex(MediaStore.Audio.Media.DURATION));
                String path = sourceCursor.getString
                        (sourceCursor.getColumnIndex(MediaStore.Audio.Media.DATA));
                int albumId = sourceCursor.getInt
                        (sourceCursor.getColumnIndex(MediaStore.Audio.Media.ALBUM_ID));
                Bitmap songImagePath = getSongImagePath(albumId,context);
                Song tempSong = new Song(name,artist,duration,path,albumId,songImagePath);
                destSongList.add(tempSong);
            }
        }
    }


}
