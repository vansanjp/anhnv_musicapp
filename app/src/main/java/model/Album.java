package model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.List;

/**
 * Created by vietanh on 9/29/16.
 */
public class Album implements Parcelable{
    private int mAlbumId;
    private String mAlbumName;
    private String mAlbumArtist;
    private List<Song> mSongList;

    public Album(int albumId, String albumName, String albumArtist, List<Song> songList){
        mAlbumId = albumId;
        mAlbumName = albumName;
        mAlbumArtist = albumArtist;
        mSongList = songList;
    }

    public Album(Parcel in){
        mAlbumId = in.readInt();
        mAlbumName = in.readString();
        mAlbumArtist = in.readString();
        in.readList(mSongList,getClass().getClassLoader());
    }

    public String getAlbumName() {
        return mAlbumName;
    }

    public String getAlbumArtist() {
        return mAlbumArtist;
    }

    public int getAlbumId() {
        return mAlbumId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(mAlbumId);
        parcel.writeString(mAlbumName);
        parcel.writeString(mAlbumArtist);
        parcel.writeList(mSongList);
    }

    @SuppressWarnings("albumtype")
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator(){

        @Override
        public Album createFromParcel(Parcel parcel) {
            return new Album(parcel);
        }

        @Override
        public Album[] newArray(int i) {
            return new Album[i];
        }
    };
}
