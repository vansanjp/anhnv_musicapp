package model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.List;

/**
 * Created by vietanh on 10/5/16.
 */
public class Artist implements Parcelable{
    private String mName;
    private List<Song> mSongList;
    private List<Album> mAlbumList;

    public Artist(Parcel in){
        mName = in.readString();
        in.readList(mSongList,List.class.getClassLoader());
        in.readList(mAlbumList,List.class.getClassLoader());
    }

    public Artist(String name, List<Song> songList, List<Album> albumList){
        mName = name;
        mSongList = songList;
        mAlbumList = albumList;
    }

    public String getArtistName(){
        return mName;
    }

    public List<Song> getSongList(){
        return mSongList;
    }

    public List<Album> getAlbumList(){
        return mAlbumList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(mName);
        parcel.writeList(mSongList);
        parcel.writeList(mAlbumList);
    }

    @SuppressWarnings("artisttypes")
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator(){
        public Artist createFromParcel(Parcel in){
            return new Artist(in);
        }

        @Override
        public Artist[] newArray(int i) {
            return new Artist[i];
        }
    };
}
