package model;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.security.AlgorithmConstraints;

/**
 * Created by vietanh on 9/22/16.
 */
public class Song implements Parcelable{
    private String mName;
    private String mArtist;
    private int mDuration;
    private String mPath;
    private int mAlbumId;
    private Bitmap mImagePath;

    public Song(String name, String artist, int duration, String path, int albumId, Bitmap imagePath){
        mName = name;
        mArtist = artist;
        mDuration = duration;
        mPath = path;
        mAlbumId = albumId;
        mImagePath = imagePath;
    }

    public Song(Parcel in){
        mName = in.readString();
        mArtist = in.readString();
        mDuration = in.readInt();
        mPath = in.readString();
        mAlbumId = in.readInt();
        mImagePath = in.readParcelable(getClass().getClassLoader());
    }

    public String getName() {
        return mName;
    }

    public String getArtist() {
        return mArtist;
    }

    public int getAlbumId() {
        return mAlbumId;
    }

    public int getDuration() {
        return mDuration;
    }

    public String getPath() {
        return mPath;
    }

    public Bitmap getImagePath(){
        return mImagePath;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(mName);
        parcel.writeString((mArtist));
        parcel.writeInt(mDuration);
        parcel.writeString(mPath);
        parcel.writeInt(mAlbumId);
        parcel.writeParcelable(mImagePath,i);
    }

    @SuppressWarnings("songtypes")
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator(){
        public Song createFromParcel(Parcel in){
            return new Song(in);
        }

        @Override
        public Artist[] newArray(int i) {
            return new Artist[i];
        }
    };
}
